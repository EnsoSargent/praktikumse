import { Router } from "express";

export const router = Router();

router.get("/sum/:a/:b", (req, res) => {
  // TODO
  // This line is needed to run tests properly
  res.status(400).send({ msg: "Sum: "+String(req.params.a + req.params.b) });
});
